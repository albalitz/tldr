# tldr
Too long; Didn't read.  
A very simple automatic text summarizer based on word frequencies and word importance to the text.


## Develop
1. (Optional) Create a virtual environment for this application: `pyvenv venv --upgrade`  
  If you use a venv, use the binaries in venv/bin in the following steps.
1. Install the required pip packages: `pip install -r requirements.txt`
1. Follow the instructions on http://www.nltk.org/data.html to install the brown corpus


## Resources
- http://www.nltk.org

### Worth reading
- https://en.wikipedia.org/wiki/Automatic_summarization
- https://en.wikipedia.org/wiki/Tf–idf
- https://np.reddit.com/r/autotldr/comments/31bfht/theory_autotldr_concept/
- https://en.wikipedia.org/wiki/Sentence_extraction
