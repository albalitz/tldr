import os
from nltk.corpus import brown as _corpus  # if you want to use another corpus, change it here


_path = os.path.dirname(__file__)
WORD_FREQUENCY_FILE = os.path.join("frequencies", "corpus_frequencies.json")

CORPUS = _corpus
CORPUS_WORD_MULTIPLIER = 0.01  # used to take their general frequency in the language into account.
HIGHEST_RANKED_SENTENCES_COUNT = 20  # number of sentences contained in the summary
