#!/usr/env/bin python3
from argparse import ArgumentParser
import logging.config
from collections import OrderedDict

from nltk.tokenize import punkt

import frequency
from utils import lemmas_from_sentence, clean_sentence, clean_word
from consts import HIGHEST_RANKED_SENTENCES_COUNT

LOG_FORMAT = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
DATE_FORMAT = "%Y-%m-%d %H:%M:%S"
DEFAULT_LOGLEVEL = logging.WARNING
logger = logging.getLogger(__name__)


def main(text):
    logger.info("Splitting text into sentences...")
    sentence_tokenizer = punkt.PunktSentenceTokenizer()
    sentences = sentence_tokenizer.sentences_from_text(text)
    clean_sentences = [clean_sentence(sentence) for sentence in sentences]
    logger.debug("Sentences: {}\n".format(sentences))

    logger.info("Splitting sentences into words...")
    words = [clean_word(word) for sentence in clean_sentences for word in sentence.split(" ") if word]
    logger.debug("Words: {}\n".format(words))

    logger.info("Counting word frequencies...")
    word_frequencies = frequency.count_word_frequencies(words)
    corpus_word_frequencies = frequency.corpus_frequencies()

    logger.info("Calculating scores for word...")
    word_scores = {}
    for word in word_frequencies.keys():
        word_score = frequency.word_score(word, word_frequencies, corpus_word_frequencies)
        word_scores[word] = word_score
    logger.debug("Word scores: {}\n".format(word_scores))

    logger.info("Calculating scores for sentences...")
    sentence_scores = OrderedDict()
    for sentence in clean_sentences:
        sentence_score = 0
        for word in lemmas_from_sentence(sentence):
            try:
                sentence_score += word_scores[clean_word(word)]
            except KeyError:
                continue

        sentence_scores[sentence] = sentence_score
    logger.debug("Sentence scores: {}\n".format(sentence_scores))

    logger.info("Filtering important sentences...")
    important_sentences = [
        s[0] for s in sorted(sentence_scores.items(),
                          key=lambda item: item[1],  # sort by sentence score
                          reverse=True)
    ][:HIGHEST_RANKED_SENTENCES_COUNT]
    logger.debug("Important sentences: {}\n".format(important_sentences))

    logger.info("Summarizing...")
    summary = [sentence for sentence in sentences if clean_sentence(sentence) in important_sentences]
    return summary


if __name__ == '__main__':
    argparser = ArgumentParser(description="Text summarizer.")
    argparser.add_argument("-v", "--verbose",
                           action="count",
                           help="Increase the loglevel. -v = INFO, -vv = DEBUG")
    argparser.add_argument("file",
                           nargs=1,
                           help="File containing text you want summarized.")
    args = argparser.parse_args()

    # see https://docs.python.org/3.5/library/logging.html#logging-levels
    log_level = DEFAULT_LOGLEVEL - args.verbose * 10 if args.verbose else DEFAULT_LOGLEVEL
    logging.basicConfig(level=log_level, format=LOG_FORMAT, datefmt=DATE_FORMAT)

    filepath = "".join(args.file)
    text = ""
    try:
        with open(filepath, "r") as f:
            text = " ".join(f.readlines())
    except FileNotFoundError as fnfe:
        logger.error(fnfe)
    else:
        summary = main(text)
        print(" ".join(summary))
