import nltk

from utils import read_saved_corpus_frequencies, save_corpus_frequencies_to_file
from consts import CORPUS, CORPUS_WORD_MULTIPLIER
from exceptions import NoFrequenciesKnownError

import logging
logger = logging.getLogger(__name__)


def word_score(word, text_frequencies, corpus_frequencies):
    return text_frequencies.get(word, 0) - CORPUS_WORD_MULTIPLIER * corpus_frequencies.get(word, 0)


def count_word_frequencies(words):
    """
    Calculates the frequencies of words in a list of words.

    To get a sorted list by frequency, use `nltk.FreqDist(count_word_frequencies(words)).most_common()`
    See this stackoverflow answer: http://stackoverflow.com/a/31850067
    """
    lemmatizer = nltk.stem.WordNetLemmatizer()
    lemmas = [lemmatizer.lemmatize(word) for word in words if word]

    word_frequencies = {}
    for lemma in lemmas:
        word_frequency = word_frequencies.get(lemma, 0)
        word_frequencies[lemma] = word_frequency + 1

    return word_frequencies


def corpus_frequencies():
    """
    Tries to read the saved word frequencies from the file.
    If it doesn't exist yet, counts the word frequencies from the corpus defined in the consts module.
    """
    corpus_word_frequencies = {}
    try:
        corpus_word_frequencies = read_saved_corpus_frequencies()
    except NoFrequenciesKnownError as nfke:
        corpus_word_frequencies = count_word_frequencies(CORPUS.words())

        save_corpus_frequencies_to_file(corpus_word_frequencies)

    return corpus_word_frequencies
