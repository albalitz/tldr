import json
import nltk
import re

from consts import WORD_FREQUENCY_FILE
from exceptions import NoFrequenciesKnownError

import logging
logger = logging.getLogger(__name__)


def clean_word(word):
    word = re.sub(r'[^\w]$', "", word)
    return word.strip().lower()


def clean_sentence(sentence):
    """
    Cleans a sentence from e.g. trailing punctuation.
    """
    if re.search(r'[^\w\s]$', sentence):
        sentence = sentence[:-1]
    return sentence.strip()


def lemmas_from_sentence(sentence):
    sentence = clean_sentence(sentence)
    lemmatizer = nltk.stem.WordNetLemmatizer()
    lemmas = [lemmatizer.lemmatize(word) for word in sentence.split(" ")]
    return [l for l in lemmas if l]


def read_saved_corpus_frequencies():
    word_frequencies = {}
    try:
        with open(WORD_FREQUENCY_FILE, "r") as f:
            word_frequencies = json.loads("".join(f.readlines()))
    except FileNotFoundError as fnfe:
        logger.warning("The corpus word frequency file does not exist yet.")
        raise NoFrequenciesKnownError
    else:
        return word_frequencies


def save_corpus_frequencies_to_file(word_frequencies):
    with open(WORD_FREQUENCY_FILE, "w+") as f:
        f.write(json.dumps(word_frequencies, sort_keys=True, indent=4))
