class NoFrequenciesKnownError(Exception):
    """
    Raised, when the file containing the corpus frequency count doesn't exist.
    """
    pass
